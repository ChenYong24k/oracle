# 基于Oracle数据库的商品销售系统的数据库设计方案

[TOC]

## 一.表空间设计方案：

- 表空间1：主表空间（用于存放核心数据表）
- 表空间2：索引表空间（用于存放索引表）

### 1.1表设计方案： 

#### 	表空间1中的表：

- 表1: customers（顾客信息表）
- 表2: products（商品信息表）

####     表空间2中的表：

- 表3: orders（订单信息表）
- 表4: order_items（订单项信息表）

代码实现:

```sql
-- 创建顾客信息表
CREATE TABLE customers (
  customer_id NUMBER PRIMARY KEY,
  customer_name VARCHAR2(100),
  address VARCHAR2(100)
);

-- 创建商品信息表
CREATE TABLE products (
  product_id NUMBER PRIMARY KEY,
  product_name VARCHAR2(100),
  price NUMBER
);

-- 创建订单信息表
CREATE TABLE orders (
  order_id NUMBER PRIMARY KEY,
  customer_id NUMBER,
  order_date DATE,
  FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);

-- 创建订单项信息表
CREATE TABLE order_items (
  order_item_id NUMBER PRIMARY KEY,
  order_id NUMBER,
  product_id NUMBER,
  quantity NUMBER,
  FOREIGN KEY (order_id) REFERENCES orders(order_id),
  FOREIGN KEY (product_id) REFERENCES products(product_id)
);

```

运行结果：

![image-20230516110211895](https://cy1314.oss-cn-chengdu.aliyuncs.com/img/202305161102091.png)

### 1.2模拟数据生成：

>  为了达到不少于10万条的模拟数据量，可以使用以下代码生成模拟数据：
>
> ```sql
> -- 生成顾客信息数据
> INSERT INTO customers (customer_id, customer_name, address)
> SELECT level, 'Customer ' || level, 'Address ' || level
> FROM dual
> CONNECT BY level <= 50000;
> 
> -- 生成商品信息数据
> INSERT INTO products (product_id, product_name, price)
> SELECT level, 'Product ' || level, level * 10
> FROM dual
> CONNECT BY level <= 10000;
> 
> -- 生成订单信息数据
> INSERT INTO orders (order_id, customer_id, order_date)
> SELECT level, ROUND(DBMS_RANDOM.VALUE(1, 50000)), SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 365))
> FROM dual
> CONNECT BY level <= 100000;
> 
> -- 生成订单项信息数据
> INSERT INTO order_items (order_item_id, order_id, product_id, quantity)
> SELECT level, ROUND(DBMS_RANDOM.VALUE(1, 100000)), ROUND(DBMS_RANDOM.VALUE(1, 10000)), ROUND(DBMS_RANDOM.VALUE(1, 10))
> FROM dual
> CONNECT BY level <= 200000;
> 
> ```
>
> 

运行截图：

![image-20230516110440321](https://cy1314.oss-cn-chengdu.aliyuncs.com/img/202305161104438.png)

## 二.权限及用户分配方案

> **创建两个用户，sales_user和admin_user，并为每个用户分配了适当的权限。sales_user和admin_user都具有创建会话、表、序列和存储过程的权限。sales_user被分配到主表空间tablespace1，而admin_user被分配到主表空间tablespace1和索引表空间tablespace2。**

###      2.1用户1：sales_user

- 创建用户1：sales_user

```sql
CREATE USER sales_user IDENTIFIED BY password DEFAULT TABLESPACE tablespace1;
```

- 分配权限给用户1：sales_user

```sql
GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE, CREATE PROCEDURE TO sales_user;
ALTER USER sales_user QUOTA UNLIMITED ON tablespace1;
```

运行结果：

![image-20230516110946514](https://cy1314.oss-cn-chengdu.aliyuncs.com/img/202305161109593.png)



![image-20230516111003174](https://cy1314.oss-cn-chengdu.aliyuncs.com/img/202305161110251.png)

### 	2.2用户2：admin_user

- 创建用户2：admin_user

```sql
CREATE USER admin_user IDENTIFIED BY password DEFAULT TABLESPACE tablespace1;
```

- 分配权限给用户2：admin_user

```sql
GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE, CREATE PROCEDURE TO admin_user;
ALTER USER admin_user QUOTA UNLIMITED ON tablespace1;
ALTER USER admin_user QUOTA UNLIMITED ON tablespace2;
```

运行截图：

![image-20230516111316898](https://cy1314.oss-cn-chengdu.aliyuncs.com/img/202305161113999.png)

## 三.存储过程和函数

> 程序包中包含了三个存储过程和一个函数。其中：
>
> - `calculate_order_total` 存储过程根据订单号计算订单总金额。
> - `get_order_details` 存储过程根据订单号获取订单的详细信息，包括订单信息、顾客信息和订单项信息。
> - `get_customer_order_count` 函数用于获取给定顾客的订单总数。

相关代码：

```sql
-- 创建程序包体
CREATE OR REPLACE PACKAGE BODY sales_package AS
  -- 存储过程：计算订单总金额
  PROCEDURE calculate_order_total(
    p_order_id IN orders.order_id%TYPE,
    p_total_amount OUT NUMBER
  )
  IS
    v_total_amount NUMBER := 0;
  BEGIN
    -- 在此处编写计算订单总金额的逻辑
    -- 根据订单号查询订单项信息，计算订单总金额
    SELECT SUM(quantity * price) INTO v_total_amount
    FROM order_items oi
    JOIN products p ON oi.product_id = p.product_id
    WHERE oi.order_id = p_order_id;
    
    p_total_amount := v_total_amount;
  END calculate_order_total;

  -- 存储过程：根据订单号获取订单详情
  PROCEDURE get_order_details(
    p_order_id IN orders.order_id%TYPE
  )
  IS
    v_order_id orders.order_id%TYPE;
    v_order_date orders.order_date%TYPE;
    v_customer_name customers.customer_name%TYPE;
    v_product_name products.product_name%TYPE;
    v_quantity order_items.quantity%TYPE;
    v_total_price NUMBER;
  BEGIN
    -- 在此处编写获取订单详情的逻辑
    -- 查询订单信息及相关联的顾客信息和订单项信息
    SELECT o.order_id, o.order_date, c.customer_name, p.product_name, oi.quantity, oi.quantity * p.price AS total_price
    INTO v_order_id, v_order_date, v_customer_name, v_product_name, v_quantity, v_total_price
    FROM orders o
    JOIN customers c ON o.customer_id = c.customer_id
    JOIN order_items oi ON o.order_id = oi.order_id
    JOIN products p ON oi.product_id = p.product_id
    WHERE o.order_id = p_order_id;
    
    -- 在此处使用获取到的订单详情数据进行操作，例如打印输出等
    DBMS_OUTPUT.PUT_LINE('Order ID: ' || v_order_id);
    DBMS_OUTPUT.PUT_LINE('Order Date: ' || v_order_date);
    DBMS_OUTPUT.PUT_LINE('Customer Name: ' || v_customer_name);
    DBMS_OUTPUT.PUT_LINE('Product Name: ' || v_product_name);
    DBMS_OUTPUT.PUT_LINE('Quantity: ' || v_quantity);
    DBMS_OUTPUT.PUT_LINE('Total Price: ' || v_total_price);
  END get_order_details;

  -- 函数：获取顾客的订单总数
  FUNCTION get_customer_order_count(
    p_customer_id IN customers.customer_id%TYPE
  ) RETURN NUMBER
  IS
    v_order_count NUMBER := 0;
  BEGIN
    -- 在此处编写获取顾客订单总数的逻辑
    -- 统计顾客的订单数量
    SELECT COUNT(*) INTO v_order_count
    FROM orders
    WHERE customer_id = p_customer_id;
    
    RETURN v_order_count;
  END get_customer_order_count;
END sales_package;
/

```

运行结果：

![image-20230516111614015](https://cy1314.oss-cn-chengdu.aliyuncs.com/img/202305161116115.png)

## 四.数据库的备份方案设计

> 1. 完全备份（Full Backup）：进行定期的完全备份，将数据库的所有数据和对象都备份到一个独立的备份文件中。完全备份提供了最全面的数据恢复能力。
> 2. 增量备份（Incremental Backup）：在完全备份之后，进行定期的增量备份。增量备份只备份自上次备份以来发生更改的数据和对象，从而减少备份时间和存储空间。
> 3. 日志备份（Log Backup）：启用数据库的归档日志模式，定期备份事务日志文件（Redo Log）。日志备份记录了数据库的所有事务操作，可以用于恢复到任意时间点。
> 4. 自动备份脚本：编写脚本来自动执行备份操作，包括完全备份、增量备份和日志备份。可以使用Oracle的备份工具（如RMAN）或自定义的脚本来实现备份过程。
> 5. 备份验证与恢复测试：定期验证备份文件的完整性和可用性，以确保备份的有效性。同时，进行恢复测试，验证在灾难情况下能够成功恢复数据库。
> 6. 存储备份文件：将备份文件存储在安全的位置，最好是离主数据库所在位置远离，以防止单点故障。
> 7. 定期备份文件归档：根据备份策略和存储能力，定期对备份文件进行归档和清理，以管理备份文件的数量和存储空间。
> 8. 监控与告警：设置监控和告警系统，及时检测备份过程中的错误或异常情况，并采取适当的措施进行修复。
>
> 备份策略应考虑到数据的重要性、恢复时间目标（RTO）、恢复点目标（RPO）以及可用的存储资源。同时，定期测试备份的恢复性能，以确保在灾难恢复时能够成功还原数据库。

### 4.1完全备份

> 实现了一个简单的完全备份操作，并将备份文件移动到指定的目录。

```sql
-- 创建目录对象（用于存储备份文件）
CREATE DIRECTORY backup_dir AS '/path/to/backup/directory';

-- 进行完全备份
BEGIN
  -- 设置备份文件名（包含日期和时间戳）
  DECLARE
    v_backup_file VARCHAR2(100);
  BEGIN
    v_backup_file := 'full_backup_' || TO_CHAR(SYSDATE, 'YYYYMMDD_HH24MISS') || '.dbf';
  
    -- 执行完全备份
    EXECUTE IMMEDIATE 'ALTER DATABASE BACKUP CONTROLFILE TO ''' || v_backup_file || '''';
    EXECUTE IMMEDIATE 'ALTER DATABASE BEGIN BACKUP';
    EXECUTE IMMEDIATE 'ALTER DATABASE END BACKUP';
    
    -- 移动备份文件到指定目录
    DBMS_FILE_TRANSFER.PUT_FILE(
      source_directory_object => 'DATA_PUMP_DIR',
      source_file_name => v_backup_file,
      destination_directory_object => 'BACKUP_DIR',
      destination_file_name => v_backup_file
    );
    
    DBMS_OUTPUT.PUT_LINE('Full backup completed successfully: ' || v_backup_file);
  END;
END;

```

### 4.2 Oracle RMAN

#### 	4.2.1创建RMAN备份脚本文件（backup_script.rman）：

```sql
RUN {
  ALLOCATE CHANNEL ch1 DEVICE TYPE DISK;
  BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;
  RELEASE CHANNEL ch1;
}
```

#### 	4.2.2执行RMAN备份脚本：

```sql
RMAN TARGET /
@/path/to/backup_script.rman
```

> **上述备份脚本将执行整个数据库的备份，包括数据文件和归档日志文件，并使用压缩备份集（Compressed Backupset）进行备份。**
>
> **请注意，执行RMAN备份需要具备适当的权限和配置，以及正确设置RMAN的相关参数。确保在执行备份操作之前，您已经熟悉了RMAN的使用和相关文档，并根据实际需求进行适当的调整和配置。备份文件的存储位置和归档策略等也需要根据实际情况进行设置。**

## 五.设计总结

> **通过本次设计，成功实现了基于Oracle数据库的商品销售系统的核心功能。数据库表设计合理，满足系统的需求，并通过用户权限分配实现了安全访问控制。存储过程和函数的设计提供了复杂业务逻辑的实现，增强了系统的灵活性和性能。备份方案能够有效保护数据库的数据，并在灾难恢复时提供可靠的数据恢复手段。**
>
> **然而，设计中仍有一些改进的空间。例如，可以进一步优化数据库的性能和索引设计，加强数据安全性，实现更高级的业务功能等。此外，备份方案也可以根据具体需求进行进一步的完善和扩展。**
>
> **总体而言，通过本次设计，我们成功实现了基于Oracle数据库的商品销售系统的核心功能，并建立了合理的数据库表结构、用户权限分配方案、存储过程和函数，以及备份方案。这些设计和实现为商品销售系统的正常运行和数据安全提供了坚实的基础。**
>
> **在数据库表设计方面，我们创建了合适的表以存储顾客、商品、订单和订单项的相关信息。这些表之间建立了适当的关联关系，保证了数据的一致性和完整性。通过定义主键、外键和约束等，我们确保了数据的有效性和可靠性。**
>
> **在用户权限分配方案中，我们创建了两个用户，管理员用户和普通用户，以实现数据的安全访问和权限控制。管理员用户拥有完全访问权限，可以对所有表进行增删改查操作，而普通用户仅限于查询操作，有效保护了敏感数据的安全性。**
>
> **通过存储过程和函数的设计，我们实现了一些复杂的业务逻辑，如计算订单总金额、获取订单详情和获取顾客的订单总数等。这些存储过程和函数提供了方便的接口和可重复使用的功能，提高了系统的性能和开发效率。**
>
> **备份方案是确保数据安全和灾难恢复的关键所在。我们设计了一个简单的备份方案，包括完全备份、增量备份和日志备份。通过定期执行备份操作，我们可以将数据库的数据和日志进行有效的存储和管理，以便在需要时进行灾难恢复和数据恢复操作。**
>
> **总的来说，本次设计为基于Oracle数据库的商品销售系统提供了稳固的基础，但仍有进一步的改进和扩展空间。我们可以考虑优化数据库的性能和索引设计，增强数据的安全性，实现更高级的业务功能，以及完善备份方案来满足不同的需求和风险管理要求。通过持续的监测和优化，我们可以确保系统的稳定性、可靠性和可扩展性，提供优质的商品销售服务。**