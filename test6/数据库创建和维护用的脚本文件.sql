-- 创建顾客信息表
CREATE TABLE customers (
  customer_id NUMBER PRIMARY KEY,
  customer_name VARCHAR2(100),
  address VARCHAR2(100)
);

-- 创建商品信息表
CREATE TABLE products (
  product_id NUMBER PRIMARY KEY,
  product_name VARCHAR2(100),
  price NUMBER
);

-- 创建订单信息表
CREATE TABLE orders (
  order_id NUMBER PRIMARY KEY,
  customer_id NUMBER,
  order_date DATE,
  FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);

-- 创建订单项信息表
CREATE TABLE order_items (
  order_item_id NUMBER PRIMARY KEY,
  order_id NUMBER,
  product_id NUMBER,
  quantity NUMBER,
  FOREIGN KEY (order_id) REFERENCES orders(order_id),
  FOREIGN KEY (product_id) REFERENCES products(product_id)
);

-- 生成顾客信息数据
INSERT INTO customers (customer_id, customer_name, address)
SELECT level, 'Customer ' || level, 'Address ' || level
FROM dual
CONNECT BY level <= 50000;

-- 生成商品信息数据
INSERT INTO products (product_id, product_name, price)
SELECT level, 'Product ' || level, level * 10
FROM dual
CONNECT BY level <= 10000;

-- 生成订单信息数据
INSERT INTO orders (order_id, customer_id, order_date)
SELECT level, ROUND(DBMS_RANDOM.VALUE(1, 50000)), SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 365))
FROM dual
CONNECT BY level <= 100000;

-- 生成订单项信息数据
INSERT INTO order_items (order_item_id, order_id, product_id, quantity)
SELECT level, ROUND(DBMS_RANDOM.VALUE(1, 100000)), ROUND(DBMS_RANDOM.VALUE(1, 10000)), ROUND(DBMS_RANDOM.VALUE(1, 10))
FROM dual
CONNECT BY level <= 200000;

-- 创建用户1：sales_user
CREATE USER sales_user IDENTIFIED BY password DEFAULT TABLESPACE tablespace1;
-- 分配权限给用户1：sales_user
GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE, CREATE PROCEDURE TO sales_user;
ALTER USER sales_user QUOTA UNLIMITED ON tablespace1;
-- 创建用户2：admin_user
CREATE USER admin_user IDENTIFIED BY password DEFAULT TABLESPACE tablespace1;
-- 分配权限给用户2：admin_user
GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE, CREATE PROCEDURE TO admin_user;
ALTER USER admin_user QUOTA UNLIMITED ON tablespace1;
ALTER USER admin_user QUOTA UNLIMITED ON tablespace2;



-- 创建程序包体
CREATE OR REPLACE PACKAGE BODY sales_package AS
  -- 存储过程：计算订单总金额
  PROCEDURE calculate_order_total(
    p_order_id IN orders.order_id%TYPE,
    p_total_amount OUT NUMBER
  )
  IS
    v_total_amount NUMBER := 0;
  BEGIN
    -- 在此处编写计算订单总金额的逻辑
    -- 根据订单号查询订单项信息，计算订单总金额
    SELECT SUM(quantity * price) INTO v_total_amount
    FROM order_items oi
    JOIN products p ON oi.product_id = p.product_id
    WHERE oi.order_id = p_order_id;
    
    p_total_amount := v_total_amount;
  END calculate_order_total;

  -- 存储过程：根据订单号获取订单详情
  PROCEDURE get_order_details(
    p_order_id IN orders.order_id%TYPE
  )
  IS
    v_order_id orders.order_id%TYPE;
    v_order_date orders.order_date%TYPE;
    v_customer_name customers.customer_name%TYPE;
    v_product_name products.product_name%TYPE;
    v_quantity order_items.quantity%TYPE;
    v_total_price NUMBER;
  BEGIN
    -- 在此处编写获取订单详情的逻辑
    -- 查询订单信息及相关联的顾客信息和订单项信息
    SELECT o.order_id, o.order_date, c.customer_name, p.product_name, oi.quantity, oi.quantity * p.price AS total_price
    INTO v_order_id, v_order_date, v_customer_name, v_product_name, v_quantity, v_total_price
    FROM orders o
    JOIN customers c ON o.customer_id = c.customer_id
    JOIN order_items oi ON o.order_id = oi.order_id
    JOIN products p ON oi.product_id = p.product_id
    WHERE o.order_id = p_order_id;
    
    -- 在此处使用获取到的订单详情数据进行操作，例如打印输出等
    DBMS_OUTPUT.PUT_LINE('Order ID: ' || v_order_id);
    DBMS_OUTPUT.PUT_LINE('Order Date: ' || v_order_date);
    DBMS_OUTPUT.PUT_LINE('Customer Name: ' || v_customer_name);
    DBMS_OUTPUT.PUT_LINE('Product Name: ' || v_product_name);
    DBMS_OUTPUT.PUT_LINE('Quantity: ' || v_quantity);
    DBMS_OUTPUT.PUT_LINE('Total Price: ' || v_total_price);
  END get_order_details;

  -- 函数：获取顾客的订单总数
  FUNCTION get_customer_order_count(
    p_customer_id IN customers.customer_id%TYPE
  ) RETURN NUMBER
  IS
    v_order_count NUMBER := 0;
  BEGIN
    -- 在此处编写获取顾客订单总数的逻辑
    -- 统计顾客的订单数量
    SELECT COUNT(*) INTO v_order_count
    FROM orders
    WHERE customer_id = p_customer_id;
    
    RETURN v_order_count;
  END get_customer_order_count;
END sales_package;
/




-- 完全备份
-- 创建目录对象（用于存储备份文件）
CREATE DIRECTORY backup_dir AS '/path/to/backup/directory';

-- 进行完全备份
BEGIN
  -- 设置备份文件名（包含日期和时间戳）
  DECLARE
    v_backup_file VARCHAR2(100);
  BEGIN
    v_backup_file := 'full_backup_' || TO_CHAR(SYSDATE, 'YYYYMMDD_HH24MISS') || '.dbf';
  
    -- 执行完全备份
    EXECUTE IMMEDIATE 'ALTER DATABASE BACKUP CONTROLFILE TO ''' || v_backup_file || '''';
    EXECUTE IMMEDIATE 'ALTER DATABASE BEGIN BACKUP';
    EXECUTE IMMEDIATE 'ALTER DATABASE END BACKUP';
    
    -- 移动备份文件到指定目录
    DBMS_FILE_TRANSFER.PUT_FILE(
      source_directory_object => 'DATA_PUMP_DIR',
      source_file_name => v_backup_file,
      destination_directory_object => 'BACKUP_DIR',
      destination_file_name => v_backup_file
    );
    
    DBMS_OUTPUT.PUT_LINE('Full backup completed successfully: ' || v_backup_file);
  END;
END;



-- Oracle RMAN
RUN {
  ALLOCATE CHANNEL ch1 DEVICE TYPE DISK;
  BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;
  RELEASE CHANNEL ch1;
}

RMAN TARGET /
@/path/to/backup_script.rman